﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {

	public Transform target;
	private Vector2 heading;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		heading = Vector2.right;
		rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		Vector3 pos = target.position;
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * 3.0f;

		if (rigidbody.position.x > 0) {
			StartCoroutine ("Defend");
		} 
	}

	IEnumerator Defend() {

		Vector3 pos = target.position;
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * 3.0f;

		rigidbody.velocity = vel;

		yield return new WaitForSeconds (3.0f);

		rigidbody.velocity = -vel;
	}

}

